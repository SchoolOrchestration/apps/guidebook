export default {
  title: 'Building a practice',
  toc: {
    chapters: [
      {
        id: 1,
        title: 'Chapter 1',
        sections: [
          {
            title: 'Section 1',
            slug: '/chapter-1/section-1/'
          },
          {
            title: 'Section 2',
            slug: '/chapter-1/section-2/'
          },
          {
            title: 'Section 2',
            slug: '/chapter-1/section-2/'
          },
          {
            title: 'Section 3',
            slug: '/chapter-1/section-3/'
          }
        ]
      },
      {
        id: 2,
        title: 'Chapter 2',
        sections: [
          {
            title: 'Section 1',
            slug: '/chapter-1/section-1/'
          },
          {
            title: 'Section 2',
            slug: '/chapter-1/section-2/'
          },
          {
            title: 'Section 2',
            slug: '/chapter-1/section-2/'
          },
          {
            title: 'Section 3',
            slug: '/chapter-1/section-3/'
          }
        ]
      }
    ]
  },
  sections: [
    {
      id: 'section-1',
      slug: '/chapter-1/section-1/',
      title: 'Section 1',
      text: `
A bunch of interesting text

* list 1
* list 2
* list 3
`,
      videos: [
        {
          title: 'Welcome to AppointmentGuru',
          url: 'https://www.youtube-nocookie.com/embed/S-RBfTBraWc',
          shortcuts: [
            {
              place: '0.34',
              text: 'Something happens'
            },
            {
              place: '1.26',
              text: 'Something else happens'
            }
          ]
        }
      ],
      embed: '',
      tags: ['foo', 'bar', 'baz', 'bus'],
      actions: [
        {
          title: 'todo 1',
          description: 'lorum ipsum dolor sit amen'
        },
        {
          title: 'todo 2',
          description: 'lorum ipsum dolor sit amen'
        },
        {
          title: 'todo 3',
          description: 'lorum ipsum dolor sit amen'
        }
      ]
    }
  ]
}
